// This file was automatically generated.  Do not modify.

'use strict';

goog.provide('Blockly.Msg.en');
goog.require('Blockly.Msg');

Blockly.Msg["CONTROL_FOREVER"] = "重复执行";
Blockly.Msg["CONTROL_REPEAT"] = "重复执行 %1 次";
Blockly.Msg["CONTROL_IF"] = "如果 %1 那么";
Blockly.Msg["CONTROL_ELSE"] = "否则";
Blockly.Msg["CONTROL_STOP"] = "停止 %1";
Blockly.Msg["CONTROL_STOP_ALL"] = "所有";
Blockly.Msg["CONTROL_STOP_THIS"] = "当前脚本";
Blockly.Msg["CONTROL_STOP_OTHER"] = "其它脚本";
Blockly.Msg["CONTROL_WAIT"] = "等待 %1 秒";
Blockly.Msg["CONTROL_WAITUNTIL"] = "一直等待直到 %1";
Blockly.Msg["CONTROL_REPEATUNTIL"] = "重复执行直到 %1";
Blockly.Msg["CONTROL_WHILE"] = "重复执行 %1";
Blockly.Msg["CONTROL_FOREACH"] = "每个 %1 在 %2";
Blockly.Msg["CONTROL_STARTASCLONE"] = "当克隆体启动时";
Blockly.Msg["CONTROL_CREATECLONEOF"] = "克隆 %1";
Blockly.Msg["CONTROL_DELETETHISCLONE"] = "删除克隆体";
Blockly.Msg["CONTROL_COUNTER"] = "计数器";
Blockly.Msg["CONTROL_INCRCOUNTER"] = "增加计数器";
Blockly.Msg["CONTROL_CLEARCOUNTER"] = "清除计数器";
Blockly.Msg["DATA_SETVARIABLETO"] = "设置变量 %1 为 %2";
Blockly.Msg["DATA_CHANGEVARIABLEBY"] = "将变量 %1 增加 %2";
Blockly.Msg["DATA_SHOWVARIABLE"] = "显示变量 %1";
Blockly.Msg["DATA_HIDEVARIABLE"] = "隐藏变量 %1";
Blockly.Msg["DATA_ADDTOLIST"] = "将 %1 添加到列表 %2 后面";
Blockly.Msg["DATA_DELETEOFLIST"] = "删除列表 %2 的第 %1 项";
Blockly.Msg["DATA_INSERTATLIST"] = "将 %1 插入列表 %3 第 %2 项后面";
Blockly.Msg["DATA_REPLACEITEMOFLIST"] = "将列表 %3 的第 %2 项替换成 %1";
Blockly.Msg["DATA_ITEMOFLIST"] = "列表 %2 的第 %1 项";
Blockly.Msg["DATA_LENGTHOFLIST"] = "列表 %1 的长度";
Blockly.Msg["DATA_LISTCONTAINSITEM"] = "列表 %1 包含 %2?";
Blockly.Msg["DATA_SHOWLIST"] = "显示列表 %1";
Blockly.Msg["DATA_HIDELIST"] = "隐藏列表 %1";
Blockly.Msg["EVENT_WHENFLAGCLICKED"] = "当点击 %1";
Blockly.Msg["EVENT_WHENTHISSPRITECLICKED"] = "当角色被点击";
Blockly.Msg["EVENT_WHENSTAGECLICKED"] = "当舞台被点击";
Blockly.Msg["EVENT_WHENBROADCASTRECEIVED"] = "当收到 %1";
Blockly.Msg["EVENT_WHENBACKDROPSWITCHESTO"] = "当背景切换到 %1";
Blockly.Msg["EVENT_WHENGREATERTHAN"] = "当 %1 > %2";
Blockly.Msg["EVENT_BROADCAST"] = "广播 %1";
Blockly.Msg["EVENT_BROADCASTANDWAIT"] = "广播 %1 并等待";
Blockly.Msg["EVENT_WHENKEYPRESSED"] = "当按下 %1";
Blockly.Msg["EVENT_WHENKEYPRESSED_SPACE"] = "空格";
Blockly.Msg["EVENT_WHENKEYPRESSED_LEFT"] = "左方向键";
Blockly.Msg["EVENT_WHENKEYPRESSED_RIGHT"] = "右方向键";
Blockly.Msg["EVENT_WHENKEYPRESSED_DOWN"] = "下方向键";
Blockly.Msg["EVENT_WHENKEYPRESSED_UP"] = "上方向键";
Blockly.Msg["EVENT_WHENKEYPRESSED_ANY"] = "任意键";
Blockly.Msg["LOOKS_SAYFORSECS"] = "说 %1 %2 秒";
Blockly.Msg["LOOKS_SAY"] = "说 %1";
Blockly.Msg["LOOKS_THINKFORSECS"] = "思考 %1 %2 秒";
Blockly.Msg["LOOKS_THINK"] = "思考 %1";
Blockly.Msg["LOOKS_SHOW"] = "显示";
Blockly.Msg["LOOKS_HIDE"] = "隐藏";
Blockly.Msg["LOOKS_EFFECT_COLOR"] = "颜色";
Blockly.Msg["LOOKS_EFFECT_FISHEYE"] = "鱼眼";
Blockly.Msg["LOOKS_EFFECT_WHIRL"] = "旋转";
Blockly.Msg["LOOKS_EFFECT_PIXELATE"] = "像素化";
Blockly.Msg["LOOKS_EFFECT_MOSAIC"] = "马赛克";
Blockly.Msg["LOOKS_EFFECT_BRIGHTNESS"] = "亮度";
Blockly.Msg["LOOKS_EFFECT_GHOST"] = "透明";
Blockly.Msg["LOOKS_CHANGEEFFECTBY"] = "将特效 %1 增加 %2";
Blockly.Msg["LOOKS_SETEFFECTTO"] = "将特效 %1 设成 %2";
Blockly.Msg["LOOKS_CLEARGRAPHICEFFECTS"] = "清除特效";
Blockly.Msg["LOOKS_CHANGESIZEBY"] = "将角色尺寸增加 %1";
Blockly.Msg["LOOKS_SETSIZETO"] = "将角色尺寸设置成 %1 %";
Blockly.Msg["LOOKS_SIZE"] = "大小";
Blockly.Msg["LOOKS_SWITCHCOSTUMETO"] = "将造型切换为 %1";
Blockly.Msg["LOOKS_NEXTCOSTUME"] = "下一个造型";
Blockly.Msg["LOOKS_SWITCHBACKDROPTO"] = "将背景切换成 %1";
Blockly.Msg["LOOKS_GOTOFRONTBACK"] = "切换到 %1 层";
Blockly.Msg["LOOKS_GOTOFRONTBACK_FRONT"] = "最上";
Blockly.Msg["LOOKS_GOTOFRONTBACK_BACK"] = "最下";
Blockly.Msg["LOOKS_GOFORWARDBACKWARDLAYERS"] = "%1 %2 层";
Blockly.Msg["LOOKS_GOFORWARDBACKWARDLAYERS_FORWARD"] = "上移";
Blockly.Msg["LOOKS_GOFORWARDBACKWARDLAYERS_BACKWARD"] = "下移";
Blockly.Msg["LOOKS_BACKDROPNUMBERNAME"] = "背景 %1";
Blockly.Msg["LOOKS_COSTUMENUMBERNAME"] = "造型 %1";
Blockly.Msg["LOOKS_NUMBERNAME_NUMBER"] = "编号";
Blockly.Msg["LOOKS_NUMBERNAME_NAME"] = "名字";
Blockly.Msg["LOOKS_SWITCHBACKDROPTOANDWAIT"] = "切换背景到 %1 并等待";
Blockly.Msg["LOOKS_NEXTBACKDROP"] = "下一个背景";
Blockly.Msg["MOTION_MOVESTEPS"] = "移动 %1 步";
Blockly.Msg["MOTION_TURNLEFT"] = "右转 %1 %2 度";
Blockly.Msg["MOTION_TURNRIGHT"] = "左转 %1 %2 度";
Blockly.Msg["MOTION_POINTINDIRECTION"] = "面向 %1 方向";
Blockly.Msg["MOTION_POINTTOWARDS"] = "面向 %1";
Blockly.Msg["MOTION_POINTTOWARDS_POINTER"] = "鼠标指针";
Blockly.Msg["MOTION_GOTO"] = "移到 %1";
Blockly.Msg["MOTION_GOTO_POINTER"] = "鼠标指针";
Blockly.Msg["MOTION_GOTO_RANDOM"] = "随机位置";
Blockly.Msg["MOTION_GOTOXY"] = "移到 x： %1 y： %2";
Blockly.Msg["MOTION_GLIDESECSTOXY"] = "在 %1 秒内滑行到 x: %2 y: %3";
Blockly.Msg["MOTION_GLIDETO"] = "在 %1 秒内滑行到 %2";
Blockly.Msg["MOTION_GLIDETO_POINTER"] = "鼠标指针";
Blockly.Msg["MOTION_GLIDETO_RANDOM"] = "随机位置";
Blockly.Msg["MOTION_CHANGEXBY"] = "将x坐标增加 %1";
Blockly.Msg["MOTION_SETX"] = "将x坐标设置为 %1";
Blockly.Msg["MOTION_CHANGEYBY"] = "将y坐标增加 %1";
Blockly.Msg["MOTION_SETY"] = "将y坐标设置为 %1";
Blockly.Msg["MOTION_IFONEDGEBOUNCE"] = "碰到边缘就反弹";
Blockly.Msg["MOTION_SETROTATIONSTYLE"] = "将旋转模式设置为 %1";
Blockly.Msg["MOTION_SETROTATIONSTYLE_LEFTRIGHT"] = "左右模式";
Blockly.Msg["MOTION_SETROTATIONSTYLE_DONTROTATE"] = "不旋转";
Blockly.Msg["MOTION_SETROTATIONSTYLE_ALLAROUND"] = "任意旋转";
Blockly.Msg["MOTION_XPOSITION"] = "x坐标";
Blockly.Msg["MOTION_YPOSITION"] = "y坐标";
Blockly.Msg["MOTION_DIRECTION"] = "方向";
Blockly.Msg["OPERATORS_ADD"] = "%1 + %2";
Blockly.Msg["OPERATORS_SUBTRACT"] = "%1 - %2";
Blockly.Msg["OPERATORS_MULTIPLY"] = "%1 * %2";
Blockly.Msg["OPERATORS_DIVIDE"] = "%1 / %2";
Blockly.Msg["OPERATORS_RANDOM"] = "在 %1 到 %2 之间随机选一个数";
Blockly.Msg["OPERATORS_GT"] = "%1 > %2";
Blockly.Msg["OPERATORS_LT"] = "%1 < %2";
Blockly.Msg["OPERATORS_EQUALS"] = "%1 = %2";
Blockly.Msg["OPERATORS_AND"] = "%1 与 %2";
Blockly.Msg["OPERATORS_OR"] = "%1 或 %2";
Blockly.Msg["OPERATORS_NOT"] = "%1 取反";
Blockly.Msg["OPERATORS_JOIN"] = "连接 %1 和 %2";
Blockly.Msg["OPERATORS_LETTEROF"] = "%2 的第 %1 个字符";
Blockly.Msg["OPERATORS_LENGTH"] = "%1 的长度";
Blockly.Msg["OPERATORS_CONTAINS"] = "%1 包含 %2?";
Blockly.Msg["OPERATORS_MOD"] = "%1 除以 %2 的余数";
Blockly.Msg["OPERATORS_ROUND"] = "对 %1 四舍五入";
Blockly.Msg["OPERATORS_MATHOP"] = "%2 的 %1";
Blockly.Msg["OPERATORS_MATHOP_ABS"] = "绝对值";
Blockly.Msg["OPERATORS_MATHOP_FLOOR"] = "向下取整";
Blockly.Msg["OPERATORS_MATHOP_CEILING"] = "向上取整";
Blockly.Msg["OPERATORS_MATHOP_SQRT"] = "平方根";
Blockly.Msg["OPERATORS_MATHOP_SIN"] = "正弦";
Blockly.Msg["OPERATORS_MATHOP_COS"] = "余弦";
Blockly.Msg["OPERATORS_MATHOP_TAN"] = "正切";
Blockly.Msg["OPERATORS_MATHOP_ASIN"] = "反正弦";
Blockly.Msg["OPERATORS_MATHOP_ACOS"] = "反余弦";
Blockly.Msg["OPERATORS_MATHOP_ATAN"] = "反正切";
Blockly.Msg["OPERATORS_MATHOP_LN"] = "ln";
Blockly.Msg["OPERATORS_MATHOP_LOG"] = "log";
Blockly.Msg["OPERATORS_MATHOP_EEXP"] = "e ^";
Blockly.Msg["OPERATORS_MATHOP_10EXP"] = "10 ^";
Blockly.Msg["PROCEDURES_DEFINITION"] = "define %1";
Blockly.Msg["SENSING_TOUCHINGOBJECT"] = "碰到%1?";
Blockly.Msg["SENSING_TOUCHINGOBJECT_POINTER"] = "鼠标指针";
Blockly.Msg["SENSING_TOUCHINGOBJECT_EDGE"] = "边缘";
Blockly.Msg["SENSING_TOUCHINGCOLOR"] = "碰到颜色 %1?";
Blockly.Msg["SENSING_COLORISTOUCHINGCOLOR"] = "颜色 %1 碰到 %2?";
Blockly.Msg["SENSING_DISTANCETO"] = "到 %1 的距离";
Blockly.Msg["SENSING_DISTANCETO_POINTER"] = "鼠标指针";
Blockly.Msg["SENSING_ASKANDWAIT"] = "询问 %1 并等待";
Blockly.Msg["SENSING_ANSWER"] = "回答";
Blockly.Msg["SENSING_KEYPRESSED"] = "按键 %1 按下?";
Blockly.Msg["SENSING_MOUSEDOWN"] = "鼠标按下?";
Blockly.Msg["SENSING_MOUSEX"] = "鼠标x坐标";
Blockly.Msg["SENSING_MOUSEY"] = "鼠标y坐标";
Blockly.Msg["SENSING_SETDRAGMODE"] = "设置拖拽模式 %1";
Blockly.Msg["SENSING_SETDRAGMODE_DRAGGABLE"] = "可拖拽";
Blockly.Msg["SENSING_SETDRAGMODE_NOTDRAGGABLE"] = "不可拖拽";
Blockly.Msg["SENSING_LOUDNESS"] = "响度";
Blockly.Msg["SENSING_TIMER"] = "计时器";
Blockly.Msg["SENSING_RESETTIMER"] = "计时器清零";
Blockly.Msg["SENSING_OF"] = "%2 的 %1";
Blockly.Msg["SENSING_OF_XPOSITION"] = "x坐标";
Blockly.Msg["SENSING_OF_YPOSITION"] = "y坐标";
Blockly.Msg["SENSING_OF_DIRECTION"] = "方向";
Blockly.Msg["SENSING_OF_COSTUMENUMBER"] = "造型编号";
Blockly.Msg["SENSING_OF_COSTUMENAME"] = "造型名称";
Blockly.Msg["SENSING_OF_SIZE"] = "大小";
Blockly.Msg["SENSING_OF_VOLUME"] = "音量";
Blockly.Msg["SENSING_OF_BACKDROPNUMBER"] = "背景编号";
Blockly.Msg["SENSING_OF_BACKDROPNAME"] = "背景名";
Blockly.Msg["SENSING_CURRENT"] = "当前的 %1";
Blockly.Msg["SENSING_CURRENT_YEAR"] = "年";
Blockly.Msg["SENSING_CURRENT_MONTH"] = "月";
Blockly.Msg["SENSING_CURRENT_DATE"] = "日";
Blockly.Msg["SENSING_CURRENT_DAYOFWEEK"] = "一周第几天";
Blockly.Msg["SENSING_CURRENT_HOUR"] = "时";
Blockly.Msg["SENSING_CURRENT_MINUTE"] = "分";
Blockly.Msg["SENSING_CURRENT_SECOND"] = "秒";
Blockly.Msg["SENSING_DAYSSINCE2000"] = "2000年至今的天数";
Blockly.Msg["SENSING_USERNAME"] = "用户名";
Blockly.Msg["SOUND_PLAY"] = "播放 %1";
Blockly.Msg["SOUND_PLAYUNTILDONE"] = "播放 %1 直到完成";
Blockly.Msg["SOUND_STOPALLSOUNDS"] = "停止所有声音";
Blockly.Msg["SOUND_SETEFFECTO"] = "将音效 %1 设置成 %2";
Blockly.Msg["SOUND_CHANGEEFFECTBY"] = "将音效 %1 增加 %2";
Blockly.Msg["SOUND_CLEAREFFECTS"] = "清除所有音效";
Blockly.Msg["SOUND_EFFECTS_PITCH"] = "音调";
Blockly.Msg["SOUND_EFFECTS_PAN"] = "左、右声道";
Blockly.Msg["SOUND_CHANGEVOLUMEBY"] = "音量增加 %1";
Blockly.Msg["SOUND_SETVOLUMETO"] = "音量设置成 %1%";
Blockly.Msg["SOUND_VOLUME"] = "音量";
